package cmd

import (
	"fmt"
	sqlite3 "github.com/mattn/go-sqlite3"
	"github.com/spf13/cobra"
	"gitlab.com/felbeaver/tasktrack/db"
	"log"
)

var projectCmd = &cobra.Command{
	Use:   "project [name]",
	Short: "Projects handling",
	Long:  "Project lets you manage projects (creating, removing, editing and viewing). You need to specify a children command for a spicific action. By default command will list currently created projects. If you specify project name then you'll get a information about the project.",
	Args:  cobra.MaximumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		pch, err := db.SelectAllProjects(dbo)

		if err != nil {
			log.Fatalln(err)
		}

		fmt.Println("List of all created projects:\n")
		for p := range pch {
			fmt.Printf("* %v\n", p.Name)
		}
	},
}

var projectCreateCmd = &cobra.Command{
	Use:   "create name",
	Short: "Create project",
	Long:  "Create project with given name.",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		name := args[0]
		project := &db.Project{
			Name: name,
		}

		err := dbo.Insert(project)
		if err != nil {
			if e, ok := err.(sqlite3.Error); ok {
				if e.Code == sqlite3.ErrConstraint {
					fmt.Printf("Project %v already exists.\n", name)
					return
				} else {
					log.Fatalln(err)
				}
			} else {
				log.Fatalln(err)
			}
		}

		dbo.Commit()
		fmt.Printf("Project %v was successfully added.\n", name)
	},
}

var projectRemoveCmd = &cobra.Command{
	Use:   "remove name",
	Short: "Remove project",
	Long:  "Remove project by name.",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		name := args[0]
		p, err := db.SelectProjectByName(dbo, name)

		if err != nil {
			log.Fatalln(err)
		}

		err = dbo.Remove(p)

		if err != nil {
			log.Fatalln(err)
		}

		dbo.Commit()
		fmt.Printf("Project %v was removed.\n", name)
	},
}

func init() {
	projectCmd.AddCommand(projectCreateCmd)
	projectCmd.AddCommand(projectRemoveCmd)
	rootCmd.AddCommand(projectCmd)
}
