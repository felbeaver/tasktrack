package cmd

import (
	"fmt"
	"gitlab.com/felbeaver/tasktrack/db"
	"github.com/spf13/cobra"
)

var dbo *db.Db

var rootCmd = &cobra.Command{
	Use:   "tasktrack",
	Short: "A time tracker for tasks in a project.",
	Long:  "Tasktrack is a CLI utility for time tracking of different tasks in a project.",
}

var taskCmd = &cobra.Command{
	Use:   "task",
	Short: "Task managing",
	Long:  "Task lets you manage tasks in a particular project (creating, removing, editing and viewing). You need to specify a children command for a spicific action. By default command will list tasks of the project.",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("task called")
	},
}

func init() {
	rootCmd.AddCommand(taskCmd)
}

func Execute(dbObj *db.Db) {
	dbo = dbObj
	cobra.CheckErr(rootCmd.Execute())
}
