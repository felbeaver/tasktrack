module gitlab.com/felbeaver/tasktrack

go 1.16

require (
	github.com/mattn/go-sqlite3 v1.14.7
	github.com/spf13/cobra v1.1.3
)
