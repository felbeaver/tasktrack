package main

import (
	"gitlab.com/felbeaver/tasktrack/cmd"
	"gitlab.com/felbeaver/tasktrack/db"
	"log"
)

func main() {
	mainDb := db.NewDb()

	err := mainDb.Connect()

	if err != nil {
		log.Fatalln(err)
	}
	defer mainDb.Close()

	cmd.Execute(mainDb)
}
