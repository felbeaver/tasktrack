package db

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"time"
)

type Table interface {
	Insert(tx *sql.Tx) error
	Remove(tx *sql.Tx) error
}

type NoTxBegunError struct {
	When     time.Time
	Filename string
}

func (e *NoTxBegunError) Error() string {
	return fmt.Sprintf("No transaction was begun in database (%v) at %v", e.When, e.Filename)
}

type Db struct {
	Filename string
	dsn      string
	conn     *sql.DB
	tx       *sql.Tx
	isTxDone bool
}

func (db *Db) Connect() error {
	var err error
	db.conn, err = sql.Open("sqlite3", db.dsn)

	if err == nil {
		return db.initSchema()
	}

	return err
}

func (db *Db) getRows(query string, args []interface{}) (*sql.Rows, error) {
	return db.conn.Query(query, args...)
}

func (db *Db) initSchema() error {
	_, err := db.conn.Exec(`CREATE TABLE IF NOT EXISTS project 
	   	 	        (id INTEGER PRIMARY KEY, 
		      		name VARCHAR(50) UNIQUE NOT NULL);`)
	return err
}

func (db *Db) Close() error {
	return db.conn.Close()
}

func (db *Db) beginTx() error {
	var err error
	if db.tx == nil || db.isTxDone {
		db.tx, err = db.conn.Begin()

		if err != nil {
			return err
		}

		db.isTxDone = false
	}
	return err
}

func (db *Db) Insert(table Table) error {
	if err := db.beginTx(); err != nil {
		return err
	}

	return table.Insert(db.tx)
}

func (db *Db) Remove(table Table) error {
	if err := db.beginTx(); err != nil {
		return err
	}

	return table.Remove(db.tx)
}

func (db *Db) Commit() error {
	if db.tx == nil {
		return &NoTxBegunError{
			time.Now(),
			db.Filename,
		}
	}
	return db.tx.Commit()
}

func NewDb() *Db {
	return &Db{
		Filename: "main.db",
		dsn:      "./main.db",
	}
}
