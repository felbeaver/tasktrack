package db

import (
	"database/sql"
)

const projectTableName = "project"
const projectIdColumn = "id"
const projectNameColumn = "name"

type Project struct {
	id   int64
	Name string
}

func (p *Project) Insert(tx *sql.Tx) error {
	r, err := tx.Exec("INSERT INTO "+projectTableName+"("+
		projectNameColumn+") VALUES(?);",
		p.Name)

	if err != nil {
		return err
	}

	p.id, err = r.LastInsertId()
	return err
}

func (p *Project) Remove(tx *sql.Tx) error {
	_, err := tx.Exec("DELETE FROM "+projectTableName+" WHERE "+
		projectIdColumn+"=?;", p.id)
	return err
}

func (p *Project) RowId() int64 {
	return p.id
}

func selectProjects(dbo *Db, query string, args ...interface{}) (<-chan Project, error) {
	rows, err := dbo.getRows(query, args)

	if err != nil {
		return nil, err
	}

	ch := make(chan Project)

	go func() {
		for rows.Next() {
			var (
				id int64
				name string
			)
			if err = rows.Scan(&id, &name); err != nil {
				close(ch)
			}
			ch <- Project{id, name}
		}

		close(ch)
	}()

	return ch, nil
}

func SelectAllProjects(dbo *Db) (<-chan Project, error) {
	q := "SELECT * FROM "+projectTableName+";"
	return selectProjects(dbo, q)
}

func SelectProjectByName(dbo *Db, name string) (*Project, error) {
	q := "SELECT * FROM "+projectTableName+" WHERE "+
	      projectNameColumn+"=?;"
	pch, err := selectProjects(dbo, q, name)
	
	if err == nil {
		p := <-pch
		return &p, err
	}

	return nil, err
}
