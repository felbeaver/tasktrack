# Tasktrack

Tasktrack is a command-line utility for time tracking of different tasks in a project.

## Installation

Use Go tool for source code managing. Type following command to compile and install a utility.

```bash
git clone https://gitlab.com/felbeaver/tasktrack.git
cd tasktrack
go compile
go install
```

Now the utility should be installed in your Go install directory.

## Usage

A utility has a general help information and help information about specific commands.

To get information about commands type following command. 

```bash
tasktrack [command] help
# or
tasktrack [command] --help
```

where [command] identifies a specific command for which to display help.

If you don't specify [command] argument then you'll get brief information about all commands available in the utility.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
